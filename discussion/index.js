// console.log("kawjdhwa")

//  [Section] while loop

/*

	Syntax:

	while (expression/condition){
		statement
	}
*/

/*
let count = 0;

while(count != 5){
	console.log("while: " + count);


	// iteration - increases the value of count after every iteration to stop the loop when it reachers 5.
	count++;
};
*/
// ++ - increment, -- decrement


// [SECTION]
/*
    Do while loop

    A do while loop works a lot like the while loop but unlike while loops, do while loops guarantee that the code will be executed atleast once.

    Syntax:

    do{
		statement
    } while(expression/condition);


*/

let number = Number(prompt("Give me a number: "));

do{
	console.log("Do While: " + number);

	number +=1;

}while(number <= 10);

// [SECTION] For Loops

/*

	for loop is more flexible the while and do-while loops. 
	
	Syntax:
	for(initialization: expression/condition: final expression){
		statement
	}


*/

for(let count = 0;count <= 20;count++){
	console.log(count);
}



let myString = prompt("Give me a word");

// Character in strings may be counted using the .length property.


// console.log(myString.length);
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);


for(let x = 0;x < myString.length; x++){
	console.log(myString[x]);
}

// Changing of vowels using loops

let myName = "DelIZo";

for (let i = 0; i < myName.length; i++){
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "u" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "o" 
	){
		console.log(3);
	} else {
		console.log(myName[i]);
	}
}